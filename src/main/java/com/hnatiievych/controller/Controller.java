package com.hnatiievych.controller;

public interface Controller {

    void parseByGson();
    void praseByJackson();
}
