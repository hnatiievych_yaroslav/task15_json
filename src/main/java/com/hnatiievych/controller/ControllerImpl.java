package com.hnatiievych.controller;


import com.hnatiievych.model.json.GsonParser;
import com.hnatiievych.model.json.JacksonParser;
import com.hnatiievych.model.json.Parser;
import com.hnatiievych.model.json.Validator;

import java.io.File;

public class ControllerImpl implements Controller {
    Parser parser;
    static File json = new File("src/main/resources/banks.json");
    static File newJson = new File("src/main/resources/banksTest.json");
    static File jsonSchema = new File("src/main/resources/banksSchema.json");

    public void setParser(Parser parser) {
        this.parser = parser;
    }

    @Override
    public void parseByGson() {
        if(Validator.validate(json,jsonSchema)) {
            setParser(new GsonParser());
            parser.writeJson(parser.readJson(json), newJson);
        }
    }

    @Override
    public void praseByJackson() {
        if(Validator.validate(json,jsonSchema)) {
            setParser(new JacksonParser());
            parser.writeJson(parser.readJson(json), newJson);
        }
    }
}

