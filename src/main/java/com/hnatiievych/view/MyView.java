package com.hnatiievych.view;

import com.hnatiievych.controller.Controller;
import com.hnatiievych.controller.ControllerImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {


    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();

        menu.put("1", "  1 - parse and write banks.json by Gson in banksTest.json");
        menu.put("2", "  2 - parse and write banks.json by Jackson in banksTest.json");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);


    }

    private void  pressButton1() {
        controller.parseByGson();
    }

    private void pressButton2() {
        controller.praseByJackson();
    }




    //-------------------------------------------------------------------------
    /**
     * method print menu in console
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * method for write user choose
     */

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}

