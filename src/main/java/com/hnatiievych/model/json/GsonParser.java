package com.hnatiievych.model.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.hnatiievych.model.Bank;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

public class GsonParser implements Parser{
    private Gson gson;


    public GsonParser(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.setPrettyPrinting().create();
    }

    public List<Bank> readJson(File json) {
        List<Bank> banks = null;
        try {
            Type itemsListType = new TypeToken<List<Bank>>() {
            }.getType();
            return gson.fromJson(new FileReader(json), itemsListType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return banks;
    }


    public void writeJson(List banks,File file) {
        try (FileWriter writer = new FileWriter(file)){
            writer.write(gson.toJson(banks));
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
