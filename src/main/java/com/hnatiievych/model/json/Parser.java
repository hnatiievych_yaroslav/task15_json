package com.hnatiievych.model.json;

import com.hnatiievych.model.Bank;

import java.io.File;
import java.util.List;

public interface Parser {
    public List<Bank> readJson(File json);
    void writeJson(List<Bank> banks, File file);

}
