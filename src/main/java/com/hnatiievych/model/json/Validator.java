package com.hnatiievych.model.json;


import com.fasterxml.jackson.databind.JsonNode;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;

public class Validator {
    public static boolean validate(File json, File schema) {
        try {
            final JsonNode data = JsonLoader.fromFile(json);
            final JsonNode schemaData = JsonLoader.fromFile(schema);
            final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
            JsonValidator jsonValidator = factory.getValidator();
            ProcessingReport report = jsonValidator.validate(schemaData, data);
            return report.isSuccess();
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
        return false;
    }
}