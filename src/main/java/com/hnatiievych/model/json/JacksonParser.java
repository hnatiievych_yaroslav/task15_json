package com.hnatiievych.model.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hnatiievych.model.Bank;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JacksonParser implements Parser {
    private ObjectMapper objectMapper;



    public JacksonParser(){
         this.objectMapper = new ObjectMapper();

    }


    public List<Bank> readJson(File json) {
        List<Bank> banks = null;
        try {
            banks =  Arrays.asList(objectMapper.readValue(json, Bank[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return banks;
    }

    public void writeJson(List banks,File file) {
        try{
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file,banks);
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
